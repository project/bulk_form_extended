<?php

namespace Drupal\bulk_form_extended\Plugin\views\field;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\views\Plugin\views\field\BulkForm;
use Drupal\views\ViewExecutable;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Extends the 'bulk_form_extended' plugin class.
 *
 * @ViewsField("bulk_form_extended")
 */
class BulkFormExtended extends BulkForm {

  /**
   * The token service.
   *
   * @var \Drupal\Core\Utility\Token
   */
  protected $token;

  /**
   * An array of action IDs indexed by their form-safe value.
   *
   * @var array
   */
  protected $renamed_actions;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->token = $container->get('token');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function init(ViewExecutable $view, DisplayPluginBase $display, array &$options = NULL) {
    parent::init($view, $display, $options);

    // We rename the actions to ensure that no '.' character exists, as this
    // will cause problems detecting the triggering element if multiple buttons
    // are used.
    foreach ($this->actions as $actionId => $action) {
      $this->renamed_actions[$this->safeActionId($actionId)] = $actionId;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    // @todo find a better way to find out what entity's are in this view.
    $base_tables = $this->view->getBaseTables();

    $token_types = [];

    foreach ($base_tables as $base_table => $value) {
      $token_types[] = str_replace('_field_data', '', $base_table);
    }

    $form['bulk_form_extended'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Extended options'),
      '#weight' => 10,
    ];

    $form['select_all'] = [
      '#fieldset' => 'bulk_form_extended',
      '#type' => 'checkbox',
      '#title' => $this->t('Show "Select All" button?'),
      '#description' => $this->t('Show a button which allows you to select all the items on the list. If all items are already selected, the button will become a "Deselect All" button.'),
      '#default_value' => $this->options['select_all'],
    ];

    $form['custom_empty_select_message'] = [
      '#fieldset' => 'bulk_form_extended',
      '#type' => 'checkbox',
      '#title' => $this->t('Display a custom empty select message?'),
      '#default_value' => $this->options['custom_empty_select_message'],
    ];

    $form['custom_empty_select_message_value'] = [
      '#fieldset' => 'bulk_form_extended',
      '#type' => 'textfield',
      '#title' => $this->t('Custom empty select message'),
      '#description' => $this->t('The message to return on an empty select.'),
      '#default_value' => $this->options['custom_empty_select_message_value'],
      '#states' => [
        'invisible' => [
          ':input[name="options[custom_empty_select_message]"]' => ['checked' => FALSE],
        ],
      ],
    ];

    $form['checkbox_label'] = [
      '#fieldset' => 'bulk_form_extended',
      '#type' => 'checkbox',
      '#title' => $this->t('Create a label for individual checkboxes.'),
      '#default_value' => $this->options['checkbox_label'],
    ];

    $form['checkbox_label_value'] = [
      '#fieldset' => 'bulk_form_extended',
      '#type' => 'textfield',
      '#title' => $this->t('Checkbox Label'),
      '#description' => $this->t('The label for the checkboxes. This field supports tokens.'),
      '#default_value' => $this->options['checkbox_label_value'],
      '#states' => [
        'invisible' => [
          ':input[name="options[checkbox_label]"]' => ['checked' => FALSE],
        ],
      ],
    ];

    $form['token_help'] = [
      '#fieldset' => 'bulk_form_extended',
      '#theme' => 'token_tree_link',
      '#token_types' => $token_types,
    ];

    $form['replace_action_dropdown_with_buttons'] = [
      '#fieldset' => 'bulk_form_extended',
      '#type' => 'checkbox',
      '#title' => $this->t('Replace the dropdown field with buttons.'),
      '#default_value' => $this->options['replace_action_dropdown_with_buttons'],
    ];

    parent::buildOptionsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function viewsForm(&$form, FormStateInterface $form_state) {
    parent::viewsForm($form, $form_state);

    // Only extend the bulk form if there are results.
    if (!empty($this->view->result)) {
      // Alter the checkbox labels.
      if ($this->options['checkbox_label']) {
        $this->alterCheckboxLabels($form, $form_state);
      }

      // Add the select all button.
      if ($this->options['select_all']) {
        $this->alterToggleAll($form);
      }

      // Replace the action dropdown with buttons.
      if ($this->options['replace_action_dropdown_with_buttons']) {
        $this->replaceActionDropdown($form);
      }

      // Duplicate the form actions into the action container in the header.
      $form['header'][$this->options['id']]['actions'] = $form['actions'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function viewsFormSubmit(&$form, FormStateInterface $form_state) {
    // If buttons are selected, populate the action value with the action name
    // being triggered.
    if ($this->options['replace_action_dropdown_with_buttons']) {
      $safeActionId = $form_state->getTriggeringElement()['#name'];

      // Get the original action ID.
      $actionId = $this->renamed_actions[$safeActionId];

      // Set the action value in the form state.
      $form_state->setValue('action', $actionId);
    }
    parent::viewsFormSubmit($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['select_all'] = ['default' => 0];
    $options['checkbox_label'] = ['default' => 0];
    $options['checkbox_label_value'] = ['default' => $this->t('Update this item')];
    $options['replace_action_dropdown_with_buttons'] = ['default' => 0];
    $options['custom_empty_select_message'] = ['default' => 0];
    $options['custom_empty_select_message_value'] = ['default' => $this->t('No items selected.')];
    return $options;
  }

  /**
   * Alters the form to add custom checkbox labels.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  protected function alterCheckboxLabels(&$form, FormStateInterface $form_state) {
    $use_revision = array_key_exists('revision', $this->view->getQuery()
      ->getEntityTableInfo());

    // Generate token replacements for each row.
    foreach ($this->view->result as $row_index => $row) {
      $entity = $this->getEntityTranslation($this->getEntity($row), $row);

      // Add the entity to the token data array.
      $token_data = [];
      $token_data[$row->_entity->getEntityTypeId()] = $entity;

      // Add any relationship entities to the token data array.
      foreach ($row->_relationship_entities as $field => $related_entity) {
        $token_data[$related_entity->getEntityTypeId()] = $related_entity;
      }

      $form[$this->options['id']][$row_index] = [
        '#type' => 'checkbox',
        '#title' => $this->token->replace($this->options['checkbox_label_value'], $token_data),
        '#title_display' => 'after',
        '#default_value' => !empty($form_state->getValue($this->options['id'])[$row_index]) ? 1 : NULL,
        '#return_value' => $this->calculateEntityBulkFormKey($entity, $use_revision),
      ];
    }
  }

  /**
   * Alters the form to add a toggle all button.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   */
  protected function alterToggleAll(&$form) {
    // Add the select all button.
    $form['#attached']['library'][] = 'bulk_form_extended/select_all';

    $form['actions'][$this->options['id']]['select_all'] = [
      '#type' => 'button',
      '#value' => $this->t('Select All'),
      '#attributes' => [
        'class' => ['js-bulk-form-extended-select-all'],
        'data-deselect' => [$this->t('Deselect All')],
        'data-select' => [$this->t('Select All')],
      ],
    ];
  }

  /**
   * Replaces the Action downdown selection with individual buttons.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   */
  protected function replaceActionDropdown(&$form) {
    foreach ($this->getBulkOptions() as $actionId => $actionLabel) {
      $form['actions'][$actionId] = [
        '#type' => 'submit',
        '#name' => $this->safeActionId($actionId),
        '#value' => $actionLabel,
      ];
    }

    // Remove the default submit button.
    unset($form['actions']['submit']);

    // Remove the default dropdown.
    unset($form['header'][$this->options['id']]['action']);
  }

  /**
   * {@inheritdoc}
   */
  protected function emptySelectedMessage() {
    if ($this->options['custom_empty_select_message']) {
      return $this->options['custom_empty_select_message_value'];
    }

    return parent::emptySelectedMessage();
  }

  /**
   * Helper method to get a safe action ID.
   *
   * @param string $actionId
   *   The Action ID
   *
   * @return string
   *   The safe action id.
   */
  protected function safeActionId(string $actionId) {
    return str_replace('.', '---', $actionId);
  }

}
